(setq desktop-dirname "~/.emacs.d/")
(setq default-directory "~/.emacs.d/")
;; (setq vc-handled-backends ())
(toggle-menu-bar-mode-from-frame 0)
(toggle-tool-bar-mode-from-frame 0)
(column-number-mode t)

(setq enable-recursive-minibuffers t)	;; enable recursive minibuffers
(global-set-key "\C-x\C-b" 'ibuffer) ;; make ibuffer the default
(add-hook 'python-mode-hook
	  (lambda ()
	    ;; (unless (eq buffer-file-name nil) (flymake-mode 1)) ;dont invoke flymake on temporary buffers for the interpreter
	    ;; (local-set-key [f2] 'flymake-goto-prev-error)
	    ;; (local-set-key [f3] 'flymake-goto-next-error)
	    ;; (local-set-key [f4] 'flymake-display-err-menu-for-current-line)
	    (hs-minor-mode)
	    ;; (orgtbl-mode)
	    (outline-minor-mode -1)))
(desktop-save-mode 1)
(add-to-list 'auto-mode-alist '("\\.jsx$" . javascript-mode))

(setq org-src-fontify-natively t)	;; turn on native code fontification in the Org buffer

(require 'ido)
(ido-mode t)
(icomplete-mode 99)
(if (string-equal system-type "windows-nt")

    (make-comint-in-buffer "git" nil "C:/Program Files (x86)/Git/bin/sh.exe" nil "--login" "-i")
  (message "Not Windows"))

;; Org-mode setup
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;; avoid garbage collection (default is only 400k)
(setq-default gc-cons-threshold 4000000)

;; disable scroll-bar
(scroll-bar-mode 0)

;; Save minibuffer history

(savehist-mode 1)
(setq savehist-additional-variables '(kill-ring
                                      search-ring
                                      regexp-search-ring))

;; ropemacs
;; (add-to-list 'load-path "~/AppData/Roaming/.emacs.d/elpa/pymacs-0.25")
;; (require 'pymacs)
;; (pymacs-load "ropemacs" "rope-")
;; (setq ropemacs-enable-autoimport t)
;; (setq ropemacs-enable-shortcuts nil)
;; (setq ropemacs-local-prefix "C-c C-p")

					; switch buffer switch
;; (global-set-key [C-\`] 'jedi:complete)
(global-set-key [C-tab] 'other-window)
(global-set-key [C-S-tab] (lambda() (interactive)(other-window (- 1))))


;; turn on pending delete (when a region
;; is selected, typing replaces it)
;; (delete-selection-mode nil)

(require 'saveplace)
(setq-default save-place t)
(setq save-place-file "~/.emacs.d/saved-places")

(org-babel-do-load-languages
 'org-babel-load-languages '((python . t) (R . t)))

;; custom named macro to add a docstring template
;; learnt from http://ergoemacs.org/emacs/elisp_examples.html
(defun python-insert-docstring (&key)
  "Insert docstring at point."
  (interactive "p")
  (let ((cur (point)))
    (progn
      (goto-char cur) (insert "''''''")
      (goto-char (+ cur 3))
      (indent-new-comment-line)
      (goto-char (+ cur 3))
      (indent-new-comment-line))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(before-save-hook (quote (gofmt-before-save)))
 '(custom-enabled-themes (quote (tango-dark)))
 '(display-time-24hr-format t)
 '(display-time-mode t)
 '(doc-view-ghostscript-program "gs")
 '(electric-pair-mode t)
 '(exec-path
   (quote
    ("/opt/local/bin" "/opt/local/sbin" "/opt/local/bin" "/opt/local/sbin" "/opt/local/bin" "/opt/local/sbin" "/Users/hussainparsaiyan/.gvm/bin" "/Users/hussainparsaiyan/development/smarpshare/backend/bin" "/Library/PostgreSQL/9.4/bin" "/usr/local/heroku/bin" "/Users/hussainparsaiyan/development/android-sdk-macosx/tools" "/usr/local/bin" "/usr/bin" "/bin" "/usr/sbin" "/sbin" "/usr/local/go/bin" "/Library/TeX/texbin" "/Users/hussainparsaiyan/.rvm/bin" "/usr/local/bin/" "/usr/bin/" "/Applications/Emacs.app/Contents/MacOS/bin-x86_64-10_9" "/Applications/Emacs.app/Contents/MacOS/libexec-x86_64-10_9" "/Applications/Emacs.app/Contents/MacOS/libexec" "/Applications/Emacs.app/Contents/MacOS/bin")))
 '(go-command "$GOROOT/bin/go")
 '(godef-command "godef")
 '(gofmt-command "goimports")
 '(ispell-program-name "c:/Program Files (x86)/Aspell/bin/aspell.exe")
 '(kill-ring-max 100000)
 '(org-agenda-files (quote ("path/to/org/file")))
 '(show-paren-mode t)
 '(vc-handled-backends (quote (Git))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Proxy settings
;; (setq url-proxy-services  '(("http" . "192.168.0.15:8000")
;; 			    ("https" . "192.168.0.15:8000")
;; 			    ("git" . "192.168.0.15:8000")))
;; (setenv  "http_proxy" "192.168.0.15:8000")
;; (setenv  "https_proxy" "192.168.0.15:8000")

;; (setenv "PATH"
;; 	(concat "c:/Program Files (x86)/Git/bin" ";" (getenv "PATH") ))

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
;;===== PyFlakes
;; code checking via pyflakes+flymake
;; (when (load "flymake" t)
;;   (defun flymake-pyflakes-init ()
;;     (let* ((temp-file (flymake-init-create-temp-buffer-copy
;; 		       'flymake-create-temp-inplace))
;; 	   (local-file (file-relative-name
;; 			temp-file
;; 			(file-name-directory buffer-file-name))))
;;       (list "pychecker" (list local-file))))

;;   (add-to-list 'flymake-allowed-file-name-masks
;; 	       '("\\.py\\'" flymake-pyflakes-init)))

;; (mapcar (lambda (hook) (add-hook 'find-file-hook hook))
;; 	(list 'follow-mode-hook))

;; (unload-feature 'flymake) ; unloaded in an attempt to get rid of the error
(require 'uniquify)
(setq
 uniquify-buffer-name-style
 'post-forward uniquify-separator ":")

;; (add-to-list 'load-path "~/AppData/Roaming/.emacs.d/site-lisp/python/")

;; autocomplete hook
;; (add-hook 'auto-complete-mode-hook
;; 		 (lambda ()
;; 		   (local-set-key [C-return] 'auto-complete)))

;; yassnippet
;; (add-to-list 'load-path
;;               "~/.emacs.d/plugins/yasnippet-20130218.2229")
;; (require 'yasnippet) ;; not yasnippet-bundle
;; (yas/initialize)
;; (yas/load-directory "~/.emacs.d/elpa/yasnippet-20130218.2229/snippets")


(defun revert-buffer-keep-history (&optional IGNORE-AUTO NOCONFIRM PRESERVE-MODES)
  (interactive)

  ;; tell Emacs the modtime is fine, so we can edit the buffer
  (clear-visited-file-modtime)

  ;; insert the current contents of the file on disk
  (widen)
  (delete-region (point-min) (point-max))
  (insert-file-contents (buffer-file-name))

  ;; mark the buffer as not modified
  (not-modified)
  (set-visited-file-modtime))

;; assign F5 to force revert-buffer
(global-set-key [f5] (lambda () (interactive) (revert-buffer-keep-history nil t t)))

;; vc info display in ibuffer
;; (ibuffer-vc-generate-filter-groups-by-vc-root)

;; save desktop every hour
(run-with-timer 0 (* 60 60) 'desktop-save-in-desktop-dir)


(defun delete-to-end-of-buffer ()
  (interactive)
  (set-mark-command nil)
  (end-of-buffer)
  (delete-forward-char 1))

(global-set-key [f9] 'delete-to-end-of-buffer)

;; zap before character
(defun zap-to-before-char (arg char)
  "Kill up to and ARGth occurrence of CHAR.
Case is ignored if `case-fold-search' is non-nil in the current buffer.
Goes backward if ARG is negative; error if CHAR not found."
  (interactive "p\ncZap to char: ")
  ;; Avoid "obsolete" warnings for translation-table-for-input.
  (with-no-warnings
    (if (char-table-p translation-table-for-input)
        (setq char (or (aref translation-table-for-input char) char))))
  (kill-region (point) (progn
			 (fset 'move-up
			       (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("" 0 "%d")) arg)))
                         (search-forward (char-to-string char) nil nil arg)
                         (goto-char (if (> arg 0) (1- (point)) (1+ (point))))
                         (point))))
(fset 'move-down
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([14 67108924 14] 0 "%d")) arg)))



(global-set-key (kbd "C-<") 'move-up)
(global-set-key (kbd "C->") 'move-down)

(global-unset-key "\M-z")
(global-set-key "\M-z" 'zap-to-before-char)
(put 'dired-find-alternate-file 'disabled nil)


;; ibuffer grouping
(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("emacs" (or
			 (name . "^\\*scratch\\*$")
			 (name . "^\\*Messages\\*$")))
	       ("funcmock" (or
			    (filename . "funcmock")))
	       ("smarpshare_backend" (or
				      (filename . "smarpshare\/backend")))
	       ("thesis" (or
			  (filename . "thesis")))

	       ("vc" (or
		      (name . "^\\*vc")))))))


(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-switch-to-saved-filter-groups "default")))

(require 'term)
(setq visible-bell 1)
(setq ring-bell-function `(lambda ()
			    (redraw-display)))
(server-start)

;; remove the delete-trailing-whitespace from before-save-hook hook
(remove-hook 'before-save-hook 'delete-trailing-whitespace)


;; ensure that cua-mode is disabled
(cua-mode -1)



;; package initialize on startup (http://stackoverflow.com/a/10095853)

(setq packages `(nginx-mode all android-mode anything apache-mode auctex-lua lua-mode auctex autopair batch-mode bf-mode bookmark+ bufshow coffee-mode company-go go-mode company crosshairs vline col-highlight hl-line+ csv-mode deft dired+ dired-details+ dired-details dirtree windata tree-mode djvu dockerfile-mode dos epc ctable concurrent deferred evil goto-chg undo-tree find-file-in-project ivy flycheck seq let-alist pkg-info epl dash flymake-go git-gutter+ git-commit with-editor async git-gutter go-autocomplete auto-complete popup go-complete go-direx direx go-eldoc go-gopath go-guru go-playground gotest f s go-rename go-scratch go-stacktracer golden-ratio golden-ratio-scroll-screen google-this helm helm-core icomplete+ ido-ubiquitous ido-completing-read+ ioccur isearch+ js3-mode json-mode json-snatcher json-reformat legalese less-css-mode magit magit-popup markdown-mode+ markdown-mode minimap monokai-theme multi-term multi-web-mode multiple-cursors nav paredit pdf-tools tablist sass-mode haml-mode savekill smex swiper textmate typescript-mode vlf w3 w3m web-mode yaml-mode yasnippet))

(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it is not.
  Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     ;; (package-installed-p 'evil)
     (if (package-installed-p package)
         nil
       (if 1
           (package-install package)
         package)))
   packages))

					; activate all the packages (in particular autoloads)
(package-initialize)

;; make sure to have downloaded archive description.
;; Or use package-archive-contents as suggested by Nicolas Dudebout
(or (file-exists-p package-user-dir)
    (package-refresh-contents))

(apply #'ensure-package-installed packages) ;  --> (nil nil) if iedit and magit are already installed

;; activate installed packages
(package-initialize)



;; MacBook specific
;; (setq mac-option-modifier nil
;;       mac-command-modifier 'meta
;;       x-select-enable-clipboard t) 
(if (string-equal system-type "darwin")
    (progn
      (defun aq-binding (any) nil)
      (load  "~/.emacs.d/aquamacs-tools.el")    
      (load  "~/.emacs.d/emulate-mac-keyboard-mode.el")
      (setq emulate-mac-finnish-keyboard-mode t)
      (setq mac-right-option-modifier nil)
      (emulate-mac-finnish-keyboard-mode t)))

;; (add-hook 'after-save-hook
;; 	  (lambda ()
;; 	    (if (string= major-mode "go-mode")
;; 		(progn
;; 		  (setq pnt-loc (point))
;; 		  (shell-command (format "goimports -w %s" (buffer-file-name)))
;; 		  (revert-buffer-keep-history (buffer-file-name))
;; 		  (goto-char pnt-loc)))))
(defun my-indent-or-insert-tab (arg)
  "Insert TAB if point is in a string, otherwise call
`indent-for-tab-command'."
  (interactive "P")
  (if (nth 3 (syntax-ppss (point)))
      (insert "\t")
    (indent-for-tab-command arg)))
(add-hook 'go-mode-hook
	  (lambda ()
	    (hs-minor-mode)))
(add-hook 'go-mode-hook
	  (lambda ()
	    (local-set-key
	     (kbd "TAB") #'my-indent-or-insert-tab)))
(setenv "GOPATH" "/Users/hussainparsaiyan/development/smarpshare/backend")


(global-set-key [f6] 'hs-hide-block)
(global-set-key [f7] 'hs-show-block)

(require 'go-autocomplete)
(require 'auto-complete-config)
;; (ac-config-default)
(defadvice yes-or-no-p (around prevent-dialog activate)
  "Prevent yes-or-no-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
(defadvice y-or-n-p (around prevent-dialog-yorn activate)
  "Prevent y-or-n-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))



;; enable golden ratio
;; (require 'golden-ratio)
;; (golden-ratio-mode 1)
;; (setq golden-ratio-auto-scale t)


;; disable C-w from killing the window
(setq
 original-delete-frame 'delete-frame)
(defun delete-frame (&optional FRAME FORCE) 
  (message "in my-kill-new!"))

;; multi-cursor
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)


;; js-indentation settings

(add-hook 'json-mode-hook
          (lambda ()
            (make-local-variable 'js-indent-level)
            (setq js-indent-level 2)))


;; auto-revert docs
(add-hook 'doc-view-mode-hook 'auto-revert-mode)


;; enabled delete selection
(delete-selection-mode t)

(global-set-key (kbd "M-*") 'pop-tag-mark)
;; default
;; (setq mouse-wheel-scroll-amount (2 ((shift) . 1) ((control))))
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

;; enable yas-mode globally
(yas-global-mode 1)